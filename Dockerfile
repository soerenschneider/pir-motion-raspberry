FROM python:3-slim
RUN apt update && apt -y install gcc
ADD requirements.txt /opt/motion_presence/
WORKDIR /opt/motion_presence
RUN pip3 install -r requirements.txt 

COPY motion-sensor motionsensor.py pir.py mqttbackend.py /opt/motion_presence/

RUN useradd toor
USER toor

CMD ["python3", "/opt/motion_presence/motionsensor.py"]
